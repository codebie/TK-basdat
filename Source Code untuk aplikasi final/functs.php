<?php
	function connect_db($schema = "SION") {
		$server_name = "dbpg.cs.ui.ac.id";
		$db_name = "db048";
		$username = "db048";
		$password = "jouph7Co";
		$link = pg_connect("host=$server_name dbname=$db_name user=$username password=$password");
		$query = pg_query("SET search_path to $schema");

		if(!$link)
		{
			json_response_error("Connection to db failed");
		}

		if (!$query) {
			json_response_error("set search_path failed");
		}
		
		return true;
	}

	function db_insert($table, $values) {
		$query_string = "INSERT INTO $table VALUES $values";
		return pg_query($query_string) or json_response_error($query_string . ' Insert failed');
	}

	function to_insert_value($arr) {
		$arr_len = count($arr);
		$res = "(";
		for ($x = 0; $x < $arr_len; $x++) {
			if ($x > 0) {
				$res .= ",";
			}
			$res .= "'" . $arr[$x] . "'";
		}
		$res .= ")";

		return $res;
	}

	function is_exist($table, $column, $value) {
		$query_string = "SELECT * FROM $table WHERE $column='$value'";
		$result = pg_query($query_string);

		return pg_fetch_row($result);
	}

	function query_one_object($table, $condition, $keys) {
		$query_string = "SELECT * FROM $table WHERE $condition";
		$result = pg_query($query_string);

		$ret = [];
		$fields = pg_num_fields($result);
		$row = pg_fetch_row($result);
		for ($i = 0; $i < $fields; $i++) {
			$key = pg_field_name($result, $i);
			if ($key == 'password')
				continue;
			$ret[$key] = $row[$i];
		}

		return $ret;
	}

	function generate_random_string($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function generate_random_numeric_string($length = 16) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function json_response_error($message) {
		json_response(array('status'=>'error','message'=>$message));
	}

	function json_response_success($data) {
		json_response(array('status'=>'success', 'data'=>$data));
	}

	function json_response($data) {
		header('Content-type: application/json');
		echo json_encode($data);

		exit();
	}

?>