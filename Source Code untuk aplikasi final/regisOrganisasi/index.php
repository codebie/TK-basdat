<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        li {
            list-style: none;
        }
		body {
	background: #E55D87;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to right, #5FC3E4, #E55D87);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to right, #5FC3E4, #E55D87); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
    </style>
		<title>Regis Organisasi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<?php
	include "../navbar.php";
	?>
</head>
<a href = "../index.html" style = "margin-left:35px;" class="btn btn-primary">Back?</a>
<br>
<body>
	<h2 id="later-hidden" style = "text-align : center;color:white">FORM PENDAFTARAN ORGANISASI</h2>
	<br>
	<div id="message"></div>
	<div style="width:450px; margin: auto; padding: 10px" id="updatable-container">
	<form name="insert" id="organisasi-form" action="#" method="POST">
		<title>Form Pendaftaran Organisasi</title>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="nama-organisasi">Nama Organisasi:</label>
			<input required type="text" id="nama-organisasi" name="nama-organisasi" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="website">Website:</label>
			<input required type="text" id="website" name="website" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="email">Email:</label>
			<input required type="text" id="email" name="email" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="kecamatan">Kecamatan:</label>
			<input required type="text" id="kecamatan" name="kecamatan" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="kabupaten">Kabupaten:</label>
			<input required type="text" id="kabupaten" name="kabupaten" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="provinsi">Provinsi:</label>
			<input required type="text" id="provinsi" name="provinsi" class="form-control"/>
        </div>
        <div class="form-group">
			<label style="color:white" class="col-form-label" for="kelurahan">Kelurahan:</label>
			<input required type="text" id="kelurahan" name="kelurahan" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="kode-pos">Kode Pos:</label>
			<input required type="text" id="kode-pos" name="kode-pos" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="alamat">Jalan/Desa/Kelurahan/No.Rumah:</label>
			<input required type="text" id="alamat" name="alamat" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="tujuan">Tujuan Organisasi:</label>
			<input required type="text" id="tujuan" name="tujuan" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="nama-pengurus">Nama Pengurus:</label>
			<input required type="text" id="nama-pengurus" name="nama-pengurus" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="email-pengurus">Email Pengurus:</label>
			<input required type="text" id="email-pengurus" name="email-pengurus" class="form-control"/>
        </div>
		<div class="form-group">
			<label style="color:white" class="col-form-label" for="alamat-pengurus">Alamat Pengurus:</label>
			<input required type="text" id="alamat-pengurus" name="alamat-pengurus" class="form-control"/>
        </div>
		<input type="submit" name='submit-donatur' id="submit" class="btn btn-right btn-success" style="float:right"/>
	</form>
<script>
	$("#organisasi-form").on("submit", function(){
		event.preventDefault();
		$.ajax({
			method: "POST",
			url: "submit.php",
			data: $(this).serialize(),
			success : function(response) {
				console.log(response);
				if (response.status == 'error') {
					alert(response.message);
				}else{
					$('#later-hidden').html('');
					$('#updatable-container').html('<h2 style = "text-align : center;color:white">Selamat, organisasi anda telah terdaftar</h2><br><div id="message"></div><div style="width:450px; margin: auto; padding: 10px"><ul class="list-group list-group-flush"><li style="color:white; background:transparent" class="list-group-item">Nomor registrasi organisasi: ' + response.data.nomor_registrasi + '</li><li style="color:white; background:transparent" class="list-group-item">Email pengurus: ' + response.data.email_pengurus + '</li><li style="color:white; background:transparent" class="list-group-item">Password: ' + response.data.password + '</li></ul>')
				}
			},
			error : function(error) {
				alert("Maaf belum berhasil registrasi, coba lagi")
			}
		})
	});
</script>
</body>
</html>