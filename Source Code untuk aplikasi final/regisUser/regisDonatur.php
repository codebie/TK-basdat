<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        li {
            list-style: none;
        }
		body {
	background: #E55D87;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to right, #5FC3E4, #E55D87);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to right, #5FC3E4, #E55D87); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
    </style>
		<title>Regis Donatur</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<?php
	include "../navbar.php"
	?>
</head>
<br>
<a href = "../index.html" style = "margin-left:35px;" class="btn btn-primary">Back?</a>
<br></br>
<body>
<h2 style = "text-align : center;color:white">FORM PENDAFTARAN DONATUR</h2>
<br></br>
<div id="message"></div>
<div style="width:450px; margin: auto; padding: 10px">
    <form name="insert" id="donatur_form" action="cekRegisDonatur.php" method="POST">
        <div class="form-group">
            <label class="col-form-label" for="Nama" style ="color:white">Nama:</label>
            <input type="text" id="Nama" name="Nama" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Email" style ="color:white">Email:</label>
            <input type="email" id="Email" name="Email" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Password" style ="color:white">Password:</label>
            <input type="Password" id="Password" name="Password" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kecamatan" style ="color:white">Kecamatan:</label>
            <input type="text" id="Kecamatan" name="Kecamatan" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kabupaten" style ="color:white">Kabupaten:</label>
            <input type="text" id="Kabupaten" name="Kabupaten" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Provinsi" style ="color:white">Provinsi:</label>
            <input type="text" id="Provinsi" name="Provinsi" class="form-control" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kodepos" style ="color:white">Kode Pos:</label>
            <input type="text" id="Kodepos" name="Kodepos" class="form-control" placeholder = "Contoh kode pos: 11440" required/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Deskripsi" style ="color:white">Jalan/Desa/Kelurahan/No.Rumah:</label>
            <input type="text" id="Deskripsi" name="Deskripsi" class="form-control"/>
        </div>
        <input type="submit" name='submit-donatur' id="submit" class="btn btn-right btn-success" style="float:right"/>
    </form>
	<br></br>
	<br>
</div>
</body>
</html>
