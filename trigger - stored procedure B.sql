CREATE OR REPLACE FUNCTION donatur_saldo_update()
RETURNS "trigger" AS
$$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE DONATUR SET saldo = saldo - NEW.nominal WHERE email = NEW.donatur;
RETURN NEW;
ELSIF (TG_OP = 'DELETE') THEN
UPDATE DONATUR SET saldo = saldo + OLD.nominal WHERE email = OLD.donatur;
RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
UPDATE DONATUR SET saldo = saldo + OLD.nominal WHERE email = OLD.donatur;
UPDATE DONATUR SET saldo = saldo - NEW.nominal WHERE email = NEW.email;
RETURN NEW;
END IF;
END;
$$
LANGUAGE plpgsql;


CREATE TRIGGER donatur_organisasi_saldo_update
AFTER INSERT OR DELETE OR UPDATE OF donatur, nominal
ON DONATUR_ORGANISASI
FOR EACH ROW EXECUTE PROCEDURE donatur_saldo_update();

CREATE TRIGGER donatur_kegiatan_saldo_update
AFTER INSERT OR DELETE OR UPDATE OF donatur, nominal
ON DONATUR_KEGIATAN
FOR EACH ROW EXECUTE PROCEDURE donatur_saldo_update();