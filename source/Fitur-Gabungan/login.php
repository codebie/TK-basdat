<?php
session_start();
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<script type="text/javascript" src="src/js/jquery-3.1.1.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<?php
	include "navbar.php"
	?>
	<form action="./functs.php" method="post">
		<table>
			<th>
				<td colspan="2">LOGIN SILOKER</td>
			</th>
			<tr>
				<div class="form-group">
					<td>Username : </td>
					<td><input type="text"  class="form-control" id="username" name="username" /></td>
				</tr>
			</div>
			<tr>
				<div class="form-group">
					<td>Password : </td>
					<td><input type="password"   class="form-control" id="password" name="password" /></td>
				</tr>
			</div>
			<?php
			if(isset($_SESSION["failedlog"])){
				if($_SESSION["failedlog"] === "yes") {
					?>
					<h4>Invalid credentials!</h4>
					<?php
				}
			}?>
			<tr>
			<td colspan="2"><input type="submit" id="login" name="login" value="login" class ="btn btn-default"/></td>
			</tr>
		</table>
	</form>
</body>
</html>