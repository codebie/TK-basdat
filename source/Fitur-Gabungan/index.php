<?php
include 'functs.php';
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<?php
	include "navbar.php";
	if(isset($_SESSION["username"])) {
		echo "Welcome ";
		echo $_SESSION["username"];
		echo "<br>";
		echo "Your role is ";
		echo $_SESSION["role"];
		if(isset($_SESSION["company"]))
		{
			if($_SESSION["company"] == "t"){
				echo " company";
			}
			else{
				echo " system";
			}
		}
		echo "<br>";
		//echo $_SESSION["TEST"];
	}
	?>
	<br>

</body>
</html>