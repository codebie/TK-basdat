<?php
include 'functs.php';
if(!isset($_SESSION["username"])) {
    header("Location: index.php");
    exit();
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
  <body>
    <?php
      include "navbar.php";
    ?>
    </br>
    </br>
    </br>
    <div class = "container">
      <?php
        if(isset($_SESSION["error"])){
          echo   '<div class="alert alert-danger">'.$_SESSION["error"].'</div>';
        }
        unset($_SESSION["error"]);
      ?>
      <form action="prosesNewCourse.php" method="post">
        <div class="form-group">
          <label>Course Name</label>
          <input type="text" class="form-control" placeholder="Nama Kursus" name="course_name" required>
          <small class="form-text text-muted">Nama Kursus 1-100 huruf</small>
        </div>
        <div class="form-group">
          <label>Jumlah Maksimal Murid</label>
          <input type="number" class="form-control" placeholder="Jumlah Maksimal Murid" name="maks" required>
        </div>
        <div class="form-group">
          <label>Awal pendaftaran</label>
          <input type="date" class="form-control" name="awal_daftar" required>
        </div>
        <div class="form-group">
          <label>Akhir pendaftaran</label>
          <input type="date" class="form-control" name="akhir_daftar" required>
        </div>
        <div class="form-group">
          <label>Awal kelas</label>
          <input type="date" class="form-control" name="awal_kelas" required>
        </div>
        <div class="form-group">
          <label>Akhir kelas</label>
          <input type="date" class="form-control" name="akhir_kelas" required>
        </div>
        <div class="form-group">
          <label>Kategori course</label>
          <select  class="form-control" name="kategori">
              <?php
              $query = "SELECT nama_kategori FROM KATEGORI";
              $result = pg_query($query);
              while ($row = pg_fetch_row($result)) {
                echo '<option>'.$row[0].'</option>';
              }
              ?>
           </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </body>
</html>
