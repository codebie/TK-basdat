<?php
	include "functs.php";
	$id_lowongan = $_GET['id'];
?>

<?php
	function displayLowongan($id_lowongan){
		$currentuser = $_SESSION["username"];
		$query = "select * from lowongan where lowongan_id = '$id_lowongan'";
		$result = pg_query($query);
		$rows = pg_fetch_row($result);
		$lowonganid = $rows[0];
		$lowonganname = $rows[1];
		$start_date = $rows[2];
		$end_date = $rows[3];
		$companyid = $rows[4];
		$table = "
		<h4> Lowongan: $lowonganname </h4>
		<table class='table'>
  			<tr>
  				<th></th>
  				<th></th>
  			</tr>
  			<tr>
  				<td>Nama Lowongan:		</td>
  				<td>$lowonganname</td>
  			</tr>
  			<tr>
  				<td>Tanggal Buka</td>
  				<td>$start_date</td>
  			</tr>
  			<tr>
  				<td>Tanggal Tutup</td>
  				<td>$end_date</td>
  			</tr>
  		</table>";
  		echo $table;
	}
?>

<html>
	<head>
		<meta charset="UTF-8">
		<title>SILOKER</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>

	<body>
		<?php
  		include "navbar.php";
  		$query = "select * from lowongan
  				  where lowongan_id = '$id_lowongan';";
  		$result = pg_query($query);
  		$row = pg_fetch_row($result);
  		$namaLowongan = $row[1];
  		$tanggalBuka = $row[2];
  		$tanggalTutup = $row[3];
  		?>

  		<div class="container">
  			<div class="content">
  				<h4>
	  				<div class="col-sm-100" align="center">
	  					LOWONGAN: <?php echo "$namaLowongan"; ?>
	  				</div><br>
	  				ID LOWONGAN: <?php echo "$id_lowongan"; ?><br><br>
	  				NAMA LOWONGAN: <?php echo "$namaLowongan"; ?><br><br>
	  				TANGGAL BUKA: <?php echo "$tanggalBuka"; ?><br><br>
	  				TANGGAL TUTUP: <?php echo "$tanggalTutup"; ?><br><br>
  					DAFTAR POSISI: 
  				</h4>
  				<?php 
  					modalPosisi($id_lowongan);
  					lamar();
  				?>
		  	 </div>
		  	  	<br><br><br>
  			</div>
  			<?php 
  			tambahPosisi($id_lowongan);
  			daftarPosisi($id_lowongan);
  			?>
  		</div>
	</body>
</html>