<?php
include 'functs.php';
?>
<html>
  <head>
    <meta charset="utf-8">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
      .card-block{
        border-bottom: 0.5px solid #ccc;
        padding: 10px;
      }
    </style>
</head>
  <body>
  <?php
  include 'navbar.php';
  ?>
    <div class = "container">
      </br>
      </br>
      </br>
      </br>
      <h1 class = "text-center">Online Course List</h1>
      <div class = "card text-center">
      <?php
          $type; 
          if(!isset($_SESSION['username'])){
            $type = "guest";
          }
          else if($_SESSION['role']=='user'){
            $type = "user";
            $user = $_SESSION['username'];
          }
          else{
            $type = "admin";
          }
          $query = 'SELECT NamaCourse,Nama_Kategori,Jml_peserta,Max_Peserta,Course_id FROM ONLINE_COURSE OC JOIN  KATEGORI K ON OC.kategori=K.nomor_kategori;';
          $mainResult = pg_query($query);
          while ($mainRow = pg_fetch_row($mainResult)) {
            echo '<div class = "card-block">';
            echo '<h2 class="card-title"> '.$mainRow[0].'</h2>';
            echo '<div class="card-footer">'.$mainRow[1].'</div>';
            echo '<div class="card-footer text-muted">Pendaftar : '.$mainRow[2].' orang Kapasitas : '.$mainRow[3].' orang</div>';
            $done = false;
            if($type != "admin"){
              if($type == "user"){
                  $query = "SELECT * FROM HISTORY_OC WHERE Username='".$user."' AND Course_id='".$mainRow[4]."';";
                  $result = pg_query($query);
                  $row = pg_num_rows($result);
                  if($row == 1){
                    echo '<a href="#" class="btn btn-success btn-block disabled">Sudah terdaftar</a>';
                    $done = true;
                  }
              }
              if($mainRow[2] == $mainRow[3] and !$done){
                  echo '<a href="#" class="btn btn-danger btn-block disabled">Penuh</a>';
                  $done = true;
              }
              if (!$done){
                if($type == "user"){
                  echo '<a href="prosesDaftarCourse.php?id='.$mainRow[4].'" class="btn btn-primary btn-block">Daftar</a>';
                }
                else{
                  echo '<a href="login.php" class="btn btn-primary btn-block">Daftar</a>';
                }
              }
            }
            echo '</div>';
          }
        ?>
      </div>
    </div>
  </body>
</html>