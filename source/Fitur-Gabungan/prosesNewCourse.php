<?php
include 'functs.php';
if(!isset($_SESSION["username"])) {
    header("Location: index.php");
    exit();
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
      .card-block{
        border-bottom: 0.5px solid #ccc;
        padding: 10px;
      }
    </style>
</head>
  <body>
  <?php
    include 'navbar.php';
    ?>

    <div class = "container">
        </br>
        </br>
        </br>
        </br>
        <?php
            function cleanText($data) {
              $data = trim($data);
              $data = stripslashes($data);
              $data = htmlspecialchars($data);
              return $data;
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $error = "";
                if(empty($_POST["course_name"])){
                    $error = $error."Nama Kursus tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["course_name"] = cleanText($_POST["course_name"]);
                }
                if(empty($_POST["maks"])){
                    $error = $error."Jumlah maksimal mahasiswa tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["maks"] = $_POST["maks"];
                }
                if(empty($_POST["awal_daftar"])){
                    $error = $error."Tanggal awal daftar tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["awal_daftar"] = date('d-m-Y',strtotime($_POST["awal_daftar"]));
                }
                if(empty($_POST["akhir_daftar"])){
                    $error = $error."Tanggal akhir daftar tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["akhir_daftar"] = date('d-m-Y',strtotime($_POST["akhir_daftar"]));
                }
                if(empty($_POST["awal_kelas"])){
                    $error = $error."Tanggal awal kelas tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["awal_kelas"] = date('d-m-Y',strtotime($_POST["awal_kelas"]));
                }
                if(empty($_POST["akhir_kelas"])){
                    $error = $error."Tanggal akhir daftar tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["akhir_kelas"] = date('d-m-Y',strtotime($_POST["akhir_kelas"]));
                }
                if(empty($_POST["kategori"])){
                    $error = $error."Kategori tidak boleh kosong</br>";
                }
                else{
                    $_SESSION["kategori"] = $_POST["kategori"];
                }
            }
            else{
                header("Location: newCourse.php");
                die();
            }

            if(!isset($_SESSION['username'])){
                 header("Location: index.php");
                 die();
            }

            $_SESSION["pembuat"] = $_SESSION['username'];
            $query = "SELECT no_akta FROM  PENGGUNA_ADMIN PA JOIN COMPANY C ON PA.company_pendaftar=C.no_akta WHERE username='".$_SESSION["pembuat"]."';";
            $_SESSION["akta"] = "NULL";
            $result = pg_query($query);
            while ($row = pg_fetch_row($result)) {
                $_SESSION["akta"] = "'".$row[0]."'";
            }

            if(0 == strlen($_SESSION["course_name"]) or strlen($_SESSION["course_name"]) > 100){
                $error = $error."Panjang nama kourse harus setidaknya 1 sampai 100 huruf</br>";
            }
            if(strtotime($_SESSION["awal_kelas"]) > strtotime($_SESSION["akhir_kelas"])){
                echo $_SESSION["awal_kelas"]." > ";
                echo $_SESSION["akhir_kelas"];
                echo "A";
                $error = $error."Tanggal awal kelas harus sebelum tanggal akhir kelas</br>";
            }
            if(strtotime($_SESSION["awal_daftar"]) > strtotime($_SESSION["akhir_daftar"])){
                echo $_SESSION["awal_daftar"]." > ";
                echo $_SESSION["akhir_daftar"];
                echo "B";
                $error = $error."Tanggal awal pendaftaran harus sebelum tanggal akhir pendaftaran</br>";
            }
            if(strtotime($_SESSION["awal_kelas"]) < strtotime($_SESSION["akhir_daftar"])){
                echo $_SESSION["awal_kelas"]." < ";
                echo $_SESSION["akhir_daftar"]; 
                echo "C";
                $error = $error."Kelas belum boleh mulai sebelum pendaftaran selesai</br>";
            }

            echo $_SESSION["awal_daftar"]."\n";
            echo $_SESSION["akhir_daftar"]."\n";
            echo $_SESSION["awal_kelas"]."\n";
            echo $_SESSION["akhir_kelas"]."\n";

            if($error != ""){
                $_SESSION['error'] = $error;
                header("Location: newCourse.php");
                die();
            }

            $query = "SELECT Course_id from ONLINE_COURSE ORDER BY Course_id DESC LIMIT 1;";
            $result = pg_query($query);
            $id = pg_fetch_row($result)[0]+1;
            $query = "SELECT Nomor_Kategori from KATEGORI where nama_kategori = '".$_SESSION["kategori"]."';";
            $result = pg_query($query);
            $_SESSION["kategori"] = pg_fetch_row($result)[0];

            $query = "set datestyle to dmy; INSERT INTO ONLINE_COURSE(Course_id, NamaCourse, Max_peserta, Tgl_mulai, Tgl_akhir, Tgl_awal_daftar, Tgl_akhir_daftar, Kategori, Penyedia, Pembuat) VALUES (".$id.",'".$_SESSION["course_name"]."',".$_SESSION["maks"].",'".$_SESSION["awal_kelas"]."','".$_SESSION["akhir_kelas"]."','".$_SESSION["awal_daftar"]."','".$_SESSION["akhir_daftar"]."','".$_SESSION["kategori"]."',".$_SESSION["akta"].",'".$_SESSION["pembuat"]."');";
            
            pg_query($query);
            unset($_SESSION["course_name"]);
            unset($_SESSION["maks"]);
            unset($_SESSION["awal_kelas"]);
            unset($_SESSION["akhir_kelas"]);
            unset($_SESSION["awal_daftar"]);
            unset($_SESSION["kategori"]);
            unset($_SESSION["akta"]);
            unset($_SESSION["pembuat"]);
            header("Location: myonlinecourse.php");
        ?>
    </div>
   </body>
</html>