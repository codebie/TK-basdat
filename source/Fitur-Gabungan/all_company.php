<?php
include 'functs.php';

$user = $_SESSION['username'];
$query = "SELECT * FROM pengguna_admin WHERE username = '$user'";

$result = pg_query("select * from Company");

$checkUser = pg_query($query);
$countRow = pg_num_rows($checkUser);
if ($countRow > 0){
    $_SESSION['isAdmin'] = true;
}
else {
    $_SESSION['isAdmin'] = false;
}
?>

<!DOCTYPE html >
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
        <?php
        include "navbar.php";
        ?>
<h1>Daftar Seluruh Company </h1>
<hr/>
<div>
    <table class='table table-striped' style='width:100px; margin-right:auto; margin-left: auto'>
        <thead class='thead-dark'>
        <tr>
            <th> No Akta</th>
            <th> Nama Company</th>
            <th> No Telp</th>
            <th> Nama Jalan</th>
            <th> Provinsi</th>
            <th> Kota</th>
            <th> Kode Pos</th>
            <th> Deskripsi</th>
        </tr>
        </thead>
        <?php
        while ($row = pg_fetch_row($result)) {
            echo "<tbody>";
            echo "<tr>";
            echo "<td>" . $row['0'] . "</td>";
            echo "<td>" . $row['1'] . "</td>";
            echo "<td>" . $row['2'] . "</td>";
            echo "<td>" . $row['3'] . "</td>";
            echo "<td>" . $row['4'] . "</td>";
            echo "<td>" . $row['5'] . "</td>";
            echo "<td>" . $row['6'] . "</td>";
            echo "<td>" . $row['7'] . "</td>";

            if ($row['8'] == null){
                $form = '<form method="POST">
                            <input type="hidden" name="no_akta" value='.$row['0'].'>
                            <input type="hidden" name="session_user" value="'.$_SESSION['username'].'">
                            <input type="hidden" name="unverify" value="no">
                            <input type="submit" name="verify_now" class="btn btn-success" value="Verify">
                        </form>';
               if($_SESSION['role'] == 'admin' && $_SESSION["company"] == "f") 
                    echo "<td>".$form."</td>";
            }
            else{
                $form = '<form method="POST">
                            <input type="hidden" name="no_akta" value='.$row['0'].'>
                            <input type="hidden" name="session_user" value="NULL">
                            <input type="hidden" name="unverify" value="yes">
                            <input type="submit" name="verify_now" class="btn btn-danger" value="Unverify">
                        </form>';
               if($_SESSION['role'] == 'admin' && $_SESSION["company"] == "f") 
                    echo "<td>".$form."</td>";
            }

            echo "</tr>";
            echo "</tbody>";
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST" && $_SESSION['isAdmin']){
            $user = $_POST['session_user'];
            $no_akta = $_POST['no_akta'];
            if($_POST['unverify'] != 'yes'){
            $query = "set search_path to siloker;UPDATE COMPANY SET Verified_by='$user' WHERE
            No_akta='$no_akta'";
            }
            else{
                $query = "set search_path to siloker;UPDATE COMPANY SET Verified_by= NULLIF (1, 1) WHERE
            No_akta='$no_akta'";
            }
            $query = pg_query($query);
             echo "<meta http-equiv='refresh' content='0'>";
        //41245200050
        }
        ?>
    </table>
</div>
</body>
