<?php
include 'functs.php';
?>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
  <body>
  <?php
  include 'navbar.php';
  ?>
    <div class = "container">
      </br>
      </br>
      <h2>Course Info</h2>          
      <table class="table">
        <tbody>
          <?php
            $courseid = $_GET["id"];
            $query = "SELECT * FROM ONLINE_COURSE OC JOIN  KATEGORI K ON OC.kategori=K.nomor_kategori WHERE course_id ='" . $courseid ."';";
            $result = pg_query($query);
            $row = pg_fetch_row($result);
          ?>
          <tr>
            <td class="info">Course ID</td>
            <td><?php echo $row[0]?></td>
          </tr>
          <tr>
            <td class="info">Nama Couse</td>
            <td><?php echo $row[1]?></td>
          </tr>
          <tr>
            <td class="info">Jumlah Peserta / Kapasitas</td>
            <td><?php echo $row[10]?> / <?php echo $row[2]?></td>
          </tr>
          <tr>
            <td class="info">Tanggal Daftar</td>
            <td><?php echo $row[5]?> hingga <?php echo $row[6]?></td>
          </tr>
          <tr>
            <td class="info">Tanggal Kursus</td>
            <td><?php echo $row[3]?> hingga <?php echo $row[4]?></td>
          </tr>
          <tr>
            <td class="info">Kategori</td>
            <td><?php echo $row[12]?></td>
          </tr>
          <tr>
            <td class="info">Penyedia</td>
            <td><?php echo $row[8]?></td>
          </tr>
          <tr>
            <td class="info">Pembuat</td>
            <td><?php echo $row[9]?></td>
          </tr>
        </tbody>
      </table>
      <a href=<?php echo '"coursemember.php?id='.$courseid.'"' ?> class="btn btn-primary">Cek peserta</a>
    </div>
  </body>
</html>