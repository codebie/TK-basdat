<?php
$db_connection = pg_connect("host=localhost dbname=ilhamdarmawan user=postgres password=depapepe");
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        li {
            list-style: none;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<h2>Admin Registration</h2>
<hr/>
<div id="message"></div>
<div style="width:500px; margin: auto; padding: 10px">
    <form name="insert" id="admin_form" action="index.php" method="POST">
        <div class="form-group">
            <label class="col-form-label" for="Username">Username</label>
            <input type="text" id="Username" name="Username" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Password">Password</label>
            <input type="password" id="Password" name="Password" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Nama_lengkap">Nama Lengkap</label>
            <input type="text" id="Nama_lengkap" name="Nama_lengkap" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="No_ktp">No KTP</label>
            <input type="text" id="No_ktp" name="No_ktp" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Tgl_lahir">Tgl lahir</label>
            <input type="date" id="Tgl_lahir" name="Tgl_lahir" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="No_hp">No HP</label>
            <input type="text" id="No_hp" name="No_hp" class="form-control"/>
            <div id="No_hp-warning"></div>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Nama_jalan_admin">Nama Jalan</label>
            <input type="text" id="Nama_jalan_admin" name="Nama_jalan_admin" class="form-control"/>
            <div id="postal-warning"></div>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kota_admin">Kota</label>
            <input type="text" id="Kota_admin" name="Kota_admin" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Provinsi_admin">Provinsi</label>
            <input type="text" id="Provinsi_admin" name="Provinsi_admin" class="form-control"/>
        </div>
        <div class="form-group">
            <label class="col-form-label" for="Kodepos_admin">Kode Pos</label>
            <input type="text" id="Kodepos_admin" name="Kodepos_admin" class="form-control"/>
        </div>
        <div class="form-check">
            <p><b>Company wannabe?</b></p>
            <label class="form-check-label" id="label-form"><input type="checkbox" class="form-check-input"
                                                                   id="Admin_company" name="Admin_company"
                                                                   class="form-control"/>
                True</label>
        </div>
        <?php
        $check = "SET SEARCH_PATH TO SILOKER;SELECT No_akta FROM COMPANY WHERE No_akta = '$_POST[No_akta]';";
        $result = pg_query($db_connection, $check);
        $val = pg_num_rows($result);
        if ($val == 0){
        echo "<div class='form-group'>";
            echo "<p>Masukkan No akta company dengan:". $_POST['No_akta']."</p>";
            echo "<label class='col-form-label' for='No_akta_valid'>Company</label>";
            echo "<input type='text' id='No_akta_valid' name='No_akta_valid' class='form-control'";
            echo "placeholder=".$_POST['No_akta'].">";
        echo "</div>";
        }
        else {
            echo "<div class='alert alert-danger' width='200px' style='text-align: center;'>No akta duplikat</div>";
        }
?>
        <input type="submit" name='submit' id="submit" class="btn btn-right btn-success"/>
    </form>
</div>
<script>
    $(document).ready(function () {
        $("#No_hp").on('keyup', function () {
            var no_hp = $("#No_hp").val();
            if (!$.isNumeric(no_hp) && no_hp.length > 0) {
                $("#No_hp-warning").html("No HP harus berisi angka");
                $("#No_hp-warning").removeClass().addClass("alert alert-danger");
            }
            else {
                $("#No_hp-warning").html("");
                $("#No_hp-warning").removeClass();
            }
        });
        $("#admin_form").onkeyup(function () {
            if ($("#No_akta_valid").val().trim() === '') {
                $()
            }
        });
        $("#admin_form").submit(function () {

            if ($.trim($("#Username").val()) === '' || $("#Password").val().trim() === '' || $("#Nama_lengkap").val().trim() === '' ||
                $("#No_ktp").val().trim() === '' || $("#No_hp").val().trim() === '' || $("#Tgl_lahir").val().trim() === '' ||
                $("#Nama_jalan_admin").val().trim() === '' || $("#Kodepos_admin").val().trim() === '' || $("#Kota_admin").val().trim() === '' ||
                $("#Provinsi_admin").val().trim() === '') {
                alert("Field tidak boleh ada yang kosong");
                return false;
            }
            else if (!$("#Admin_company").is(":checked")) {
                alert("Anda harus klik true");
                return false;
            }
            return true;
        });

    });
</script>
<?php
if ($val == 0) {
    $query = "SET SEARCH_PATH TO SILOKER; INSERT INTO COMPANY VALUES ('$_POST[No_akta]','$_POST[Namacompany]','$_POST[No_telp]',
        '$_POST[Nama_jalan]', '$_POST[Provinsi]','$_POST[Kota]','$_POST[Kodepos]', '$_POST[Deskripsi]', NULL);";
    $result = pg_query($db_connection, $query);
}

if (isset($_POST['submit'])) {
    $query2 = "SET SEARCH_PATH TO SILOKER; INSERT INTO PENGGUNA_ADMIN VALUES('$_POST[Username]','$_POST[Password]','$_POST[Nama_lengkap]',
        '$_POST[No_ktp]','$_POST[Tgl_lahir]','$_POST[No_hp]','$_POST[Nama_jalan_admin]','$_POST[Kota_admin]','$_POST[Provinsi_admin]','$_POST[Kodepos_admin]',
        TRUE, '$_POST[No_akta_valid]');";
    $result2 = pg_query($db_connection, $query2);
    $setAsAdmin = "SET SEARCH_PATH TO SILOKER; ALTER TABLE COMPANY SET Verified_by='$_POST[Username]' WHERE No_akta='$_POST[No_akta_valid]'";
    $result3 = pg_query($db_connection, $setAsAdmin);
}
?>
</body>
</html>
