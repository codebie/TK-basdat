<html>
  <head>
    <meta charset="utf-8">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .card-block{
        border-bottom: 0.5px solid #ccc;
        padding: 10px;
      }
    </style>
</head>
  <body>
    <div class = "container">
        </br>
        </br>
        </br>
        </br>
        <?php
            include "functs.php";
            connectdb();
            include "navbar.php";
            if(!isset($_SESSION['username'])){
                header("Location: login.php");
            }
            $user = $_SESSION['username'];
            $id = $_GET["id"];
            if($_SESSION['role'] == "admin"){
                echo "<div class='alert alert-danger'>Admin tidak diperbolehkan untuk mendaftar kursus.</div>";
                echo "Anda akan dikembalikan kembali ke course list";
                header('Refresh: 5;url=online-course.php');
            }
            else{
                $query = "SELECT max_peserta,jml_peserta FROM ONLINE_COURSE WHERE course_id='".$id."';";
                $result = pg_query($query);
                $row = pg_num_rows($result);
                $result = pg_query($query);
                $rowB = pg_fetch_row($result);
                if($row == 0){
                    echo "<div class='alert alert-danger'>Course tidak dapat ditemukan</div>";
                }
                else if($rowB[0] == $rowB[1]){
                    echo "<div class='alert alert-danger'>Course sudah penuh! Anda tidak bisa daftar</div>";
                }
                else{
                    $query = "SELECT * FROM HISTORY_OC WHERE course_id='".$id."' AND username='".$user."';";
                    $result = pg_query($query);
                    $row = pg_num_rows($result);
                    if($row == 0){
                        $query = "INSERT INTO history_oc(course_id,username,score_test,keaktifan) VALUES (".$id.",'".$user."',0,0);";
                        $result = pg_query($query);
                        echo "<div class='alert alert-success'>Anda berhasil didatarkan</div>";
                    }
                    else{
                        echo "<div class='alert alert-danger'>Anda sudah terdaftar</div>";
                    }
                }
            }
            echo "Anda akan dikembalikan kembali ke course list dalam 3 detik";
            header('Refresh: 3;url=online-course.php');
        ?>
    </div>
  </body>
</html>