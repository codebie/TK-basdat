<?php
	session_start();
	logout();
	
	
	function logout() {
		unset($_SESSION["username"]);
		unset($_SESSION["role"]);
		unset($_SESSION["donatur"]);
		unset($_SESSION["admin"]);
		unset($_SESSION["pengurus_organisasi"]);
		unset($_SESSION["relawan"]);
		unset($_SESSION["sponsor"]);
		$_SESSION["failedlog"] = null;
		header("Location: ../index.html");
		exit();
	}
?>