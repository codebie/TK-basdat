<!DOCTYPE html>
<?php
	session_start();
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        li {
            list-style: none;
        }
		body {
	background: #E55D87;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to right, #5FC3E4, #E55D87);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to right, #5FC3E4, #E55D87); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
    </style>
		<title>Profil saya</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<?php
		include "../navbar.php";
	?>
</head>
<br></br>
<body>
	<h2 style = "text-align : center;color:white">PROFIL SAYA</h2>
	<br></br>
	<div id="message"></div>
	<div style="width:450px; margin: auto; padding: 10px">
		<h1 style="color:white;text-align:center" id="later-hidden">Sedang memuat, harap tunggu</h1>
		<ul class="list-group list-group-flush" id="updatable-list">
		</ul>
<script>
	$.ajax({
		method: "POST",
		url: "submit.php",
		data: {
			<?php
				if (isset($_SESSION['role'])){
					$role = $_SESSION['role'];
					$email = $_SESSION['username'];
					echo '"role" : "' . $role . '","email" : "' . $email . '"';
				}
				else{
					echo '"role" : "norole","email":"nomail"';
				}
			?>
			
		},
		success : function(response) {
			if (response.status == 'error') {
				alert('Silakan login terlebih dahulu');
			}else{
				$('#later-hidden').html('');
				$.map(response.data, function(val, key){
					$('#updatable-list').append('<li style="color:white; background:transparent" class="list-group-item">' + key + ": " +  val + "</li>");
				});
			}
		},
		error : function(error) {
			alert("Maaf terjadi kesalahan, coba sekali lagi atau coba login ulang")
		}
	});
</script>
</body>
</html>