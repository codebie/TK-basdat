<?php
	include "../functs.php";
	connect_db();

	if (is_exist("ORGANISASI", "email_organisasi", $_POST['email']) or is_exist("ORGANISASI", "nama", $_POST['nama-organisasi'])) {
		json_response_error('Nama atau email sudah terdaftar');
	} else {	
		$insert_value = to_insert_value(array($_POST['email'], $_POST['website'], $_POST['nama-organisasi'], $_POST['provinsi'], $_POST['kabupaten'], $_POST['kecamatan'], $_POST['kelurahan'], $_POST['kode-pos'], "disetujui"));
		db_insert("ORGANISASI", $insert_value);

		$password = generate_random_string();
		$insert_value = to_insert_value(array($_POST['email-pengurus'], $password, $_POST['nama-pengurus'], $_POST['alamat-pengurus']));
		db_insert("PENGGUNA", $insert_value);

		$insert_value = to_insert_value(array($_POST['email-pengurus'], $_POST['email']));
		db_insert("PENGURUS_ORGANISASI", $insert_value);

		$nomor_registrasi = generate_random_numeric_string();
		$insert_value = to_insert_value(array($_POST['email'], $nomor_registrasi, 'aktif'));
		db_insert("ORGANISASI_TERVERIFIKASI", $insert_value);

		$result = array('nomor_registrasi' => $nomor_registrasi, 'email_pengurus' => $_POST['email-pengurus'], 'password' => $password);
		json_response_success($result);
	}
?>