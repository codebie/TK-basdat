<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" style = "float:left">SION</a>

      <?php
      if(isset($_SESSION["role"])){
        if($_SESSION["role"] === "relawan") { 
          ?>  <a class="navbar-brand" style="float:left" href="../profil/index.php">Profil</a>
          <a class="navbar-brand" style="float:left" href="">Lihat Organisasi</a>
          <a class="navbar-brand" style="float:left" href="">Berdonasi ke Organisasi</a>
      <a class="navbar-brand" style="float:left" href="">Berdonasi ke Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="../loginLogout/logout.php">Logout</a>
          <?php 
        } else if ($_SESSION["role"] === "donatur") {
          ?><a class="navbar-brand" style="float:left" href="../profil/index.php">Profil</a>
      <a class="navbar-brand" style="float:left" href="">Lihat Organisasi</a>
      <a class="navbar-brand" style="float:left" href="">Berdonasi ke Organisasi</a>
      <a class="navbar-brand" style="float:left" href="">Berdonasi ke Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="../loginLogout/logout.php" style = "align:right">Logout</a>
          <?php
        } else if ($_SESSION["role"] === "sponsor") {
          ?><a class="navbar-brand" style="float:left" href="../profil/index.php">Profil</a>
      <a class="navbar-brand" style="float:left" href="">Lihat Organisasi</a>
      <a class="navbar-brand" style="float:left" href="">Buat Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="">Lihat Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="../loginLogout/logout.php">Logout</a>
          <?php
        } else if ($_SESSION["role"] === "pengurus_organisasi") {
          ?><a class="navbar-brand" style="float:left" href="../profil/index.php">Profil</a>
      <a class="navbar-brand" style="float:left" href="">Lihat Organisasi</a>
      <a class="navbar-brand" style="float:left" href="">Buat Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="">Lihat Kegiatan</a>
      <a class="navbar-brand" style="float:left" href="../loginLogout/logout.php">Logout</a>
      <?php
    }
      } 
      ?>
    </div>
  </div>
</nav>