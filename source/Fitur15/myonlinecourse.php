<html>
  <head>
    <meta charset="utf-8">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .card-block{
        border-bottom: 0.5px solid #ccc;
        padding: 10px;
      }
    </style>
  </head>
  <body>
    </br>
    </br>
    <div class = "container">
      <h1 class = "text-center">My Course</h1>
      <div class = "card text-center">
          <?php
            include "functs.php";
            connectdb();
            include "navbar.php";
            if(!isset($_SESSION["username"])){
              header("Location: login.php");
            }
            
            if($_SESSION["role"] != "admin"){
              header("Location: index.php");
            }
            $user = $_SESSION["username"];
            $query = "SELECT NamaCourse, Nama_Kategori, Course_id, Jml_peserta, Max_peserta FROM ONLINE_COURSE OC JOIN  KATEGORI K ON OC.kategori=K.nomor_kategori WHERE pembuat ='" . $user ."';";
            $result = pg_query($query);
            while ($row = pg_fetch_row($result)) {
              echo '<div class = "card-block">';
              echo '  <h2 class="card-title"> '.$row[0].'</h2>';
              echo '  <div class="card-footer text-muted">'.$row[1].'</div>';
              echo '  <div class="row">';
              echo '      <a href="coursemember.php?id='.$row[2].'" class="btn btn-primary">Cek murid ('.$row[3].' / '.$row[4].')</a>';
              echo '      <a href="courseinfo.php?id='.$row[2].'" class="btn btn-primary">Course info</a>';
              echo '  </div>';
              echo '</div>';
            }
          ?>
          <a href="newCourse.php" class="btn btn-info">Add Course</a>
      </div>
    </div>
  </body>
</html>
