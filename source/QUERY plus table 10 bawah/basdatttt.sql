CREATE OR REPLACE FUNCTION hitung_tiket_dipesan (jadwal CHAR
(5))
RETURNS INTEGER AS
$$
DECLARE
jumlah_dipesan INTEGER;
BEGIN
SELECT COUNT(P.*) INTO jumlah_dipesan
FROM TIKET T, PEMESANAN P
WHERE T.Id_jadwal = jadwal AND P.Id_tiket =T.Id_tiket;
UPDATE JADWAL J SET jumlah_tiket_dipesan =
jumlah_dipesan
WHERE J.id_jadwal = jadwal;
RETURN jumlah_dipesan;
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION hitung_tiket_dipesan()
RETURNS void AS
$$
DECLARE
row RECORD;
BEGIN
FOR row IN
SELECT J.Id_jadwal as id_jadwal, COUNT(P.*) AS
jumlah_dipesan
FROM JADWAL J, TIKET T, PEMESANAN P
WHERE T.Id_jadwal = J.Id_jadwal AND P.Id_tiket
=T.Id_tiket
GROUP BY J.id_jadwal
LOOP
UPDATE JADWAL SET jumlah_tiket_dipesan =
row.jumlah_dipesan
WHERE Id_jadwal = row.Id_jadwal;
END LOOP;
END;
$$
LANGUAGE plpgsql;






CREATE OR REPLACE FUNCTION hitung_tiket_member()
RETURNS void AS
  $$
  DECLARE
     row RECORD;
  BEGIN
  FOR row IN
     SELECT M.email AS email, COUNT(P.*) AS jumlah_dipesan
     FROM MEMBER M, PEMESANAN P
     WHERE M.email = P.email_member
     GROUP BY M.email
  LOOP
    UPDATE MEMBER SET jumlah_tiket_dipesan = row.jumlah_dipesan
    WHERE email = row.email;
  END LOOP;
  END;
  $$
LANGUAGE plpgsql;

SELECT hitung_tiket_dipesan();

ALTER TABLE MEMBER ADD COLUMN jumlah_tiket_dipesan INT DEFAULT 0;