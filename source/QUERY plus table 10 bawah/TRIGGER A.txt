CREATE TRIGGER update_status
AFTER UPDATE
ON LAPORAN_KEUANGAN FOR EACH ROW
EXECUTE PROCEDURE update_status();

CREATE or REPLACE FUNCTION update_status()
RETURNS "trigger" AS
$$
  BEGIN
  IF(NEW.is_disetujui = 'f') THEN
    UPDATE ORGANISASI_TERVERIFIKASI
    SET status_aktif = 'tidak aktif'
    WHERE NEW.organisasi = email_organisasi;
  END IF;
  RETURN NEW;
  END;
$$
LANGUAGE plpgsql;
    