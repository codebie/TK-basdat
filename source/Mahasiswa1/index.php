<?php
session_start();
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>SILOKER</title>
	<script type="text/javascript" src="src/js/jquery-3.1.1.min.js"></script>
</head>

<body>
	<?php
	include "navbar.php";
	if(isset($_SESSION["username"])) {
		echo "Welcome ";
		echo $_SESSION["username"];
		echo "<br>";
		echo "Your role is ";
		echo $_SESSION["role"];
		if(isset($_SESSION["company"]))
		{
			if($_SESSION["company"] == "t"){
				echo " company";
			}
			else{
				echo " system";
			}
		}
		echo "<br>";
	}
	?>
	<br>

</body>
</html>