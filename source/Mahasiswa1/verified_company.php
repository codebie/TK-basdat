<?php
$db_connection = pg_connect("host=localhost dbname=ilhamdarmawan user=postgres password=depapepe");

$result = pg_query($db_connection, "set search_path to siloker; select * from Company where Verified_by IS NOT NULL;");
?>
<!DOCTYPE html >
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous">
</head>
<body>
<h1>Daftar Company Terverifikasi</h1>
<hr/>
<div>
    <table class='table table-striped' style='width:100px; margin-right:auto; margin-left: auto'>
        <thead class='thead-dark'>
        <tr>
            <th> No Akta</th>
            <th> Nama Company</th>
            <th> No Telp</th>
            <th> Nama Jalan</th>
            <th> Provinsi</th>
            <th> Kota</th>
            <th> Kode Pos</th>
            <th> Deskripsi</th>
            <th> Verified by </th>
        </tr>
        </thead>
        <?php
        while ($row = pg_fetch_row($result)) {
            echo "<tbody>";
            echo "<tr>";
            echo "<td>" . $row['0'] . "</td>";
            echo "<td>" . $row['1'] . "</td>";
            echo "<td>" . $row['2'] . "</td>";
            echo "<td>" . $row['3'] . "</td>";
            echo "<td>" . $row['4'] . "</td>";
            echo "<td>" . $row['5'] . "</td>";
            echo "<td>" . $row['6'] . "</td>";
            echo "<td>" . $row['7'] . "</td>";
            echo "<td>" . $row['8'] . "</td>";
            echo "</tr>";
            echo "</tbody>";
        }
        ?>
    </table>
</div>
</body>
